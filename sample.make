api = 2
core = 7.x

includes[] = drupal-org.make

; Custom profile
projects[sample][type] = "profile"
projects[sample][download][type] = "git"
projects[sample][download][url] = "git@gitlab.com:DavidHernandez/camp-demo.git"
projects[sample][download][branch] = "master"
